package com.patrykcd.jumpup

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.patrykcd.jumpup.helpers.Constants

fun main(arg: Array<String>)
{
    val config = LwjglApplicationConfiguration()
    config.width = Constants.WIDTH
    config.height = Constants.HEIGHT
    LwjglApplication(MyGdxGame(), config)
}