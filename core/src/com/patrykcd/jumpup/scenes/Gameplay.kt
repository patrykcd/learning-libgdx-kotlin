package com.patrykcd.jumpup.scenes

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.viewport.StretchViewport
import com.badlogic.gdx.utils.viewport.Viewport
import com.patrykcd.jumpup.helpers.Constants
import com.patrykcd.jumpup.scenes.objects.Floor
import com.patrykcd.jumpup.scenes.objects.Player
import com.patrykcd.utilities.Utils

class Gameplay(spriteBatch: SpriteBatch) : Screen
{
    private var _spriteBatch = spriteBatch

    private lateinit var _camera: OrthographicCamera
    private lateinit var _viewport: Viewport

    private lateinit var _world: World
    private lateinit var _box2DDebugRenderer: Box2DDebugRenderer

    private lateinit var _player: Player
    private lateinit var _floor: Floor

    override fun show()
    {
        _camera = OrthographicCamera()
        _viewport = StretchViewport(Constants.WIDTH.toFloat(), Constants.HEIGHT.toFloat(), _camera)

        _world = World(Vector2(0f, -9.81f), true)
        _box2DDebugRenderer = Box2DDebugRenderer()

        _player = Player(_world)
        _floor = Floor(_world)
    }

    override fun resume()
    {
    }

    override fun render(delta: Float)
    {
        // Clear
        Gdx.gl.glClearColor(0.184f, 0.310f, 0.310f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        // Update
        _world.step(delta, 6, 2)
        _player.update()
        _camera.update()

        // Camera & Scale
        val scale = _camera.combined.cpy().scl(Constants.PPM.toFloat())
        _box2DDebugRenderer.render(_world, scale)
        _spriteBatch.projectionMatrix = _camera.combined

        // Draw
        _spriteBatch.begin()
        _player.draw(_spriteBatch)
        _floor.draw(_spriteBatch)
        _spriteBatch.end()

        Utils.drawCircle(0f, 0f, Color.YELLOW, _camera.combined)
        Utils.drawLine(0f, 0f, 40f, 40f, Color.WHITE, _camera.combined)

        // Debug
        Utils.updateFreeCamera(_camera, Color.RED)
    }

    override fun resize(width: Int, height: Int)
    {
        _viewport.update(width, height, false)
    }

    override fun pause()
    {
    }

    override fun hide()
    {
    }

    override fun dispose()
    {
    }
}
