package com.patrykcd.jumpup.scenes.objects

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.World
import com.patrykcd.jumpup.helpers.Constants
import com.patrykcd.jumpup.helpers.GameObject

class Floor(world: World) : GameObject(world)
{
    init
    {
        val width = Constants.WIDTH.toFloat()
        val height = 54f

        val rect = Rectangle(
                0f,
                -(Constants.HEIGHT / 2) + height / 2,
                width,
                height)
        this.set(GameObject(rect, Color.BROWN))
        createBody(BodyDef.BodyType.KinematicBody, world)
    }

    fun setPlayer(player: Player)
    {
        val newPlayerPos = player.position
        newPlayerPos.y += (player.height / 2) + (height / 2)
        player.position = newPlayerPos
    }
}