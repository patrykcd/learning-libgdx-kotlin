package com.patrykcd.jumpup.scenes.objects

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.World
import com.patrykcd.jumpup.helpers.GameObject

class Player(world: World) : GameObject(world)
{
    init
    {
        val rect = Rectangle(0f, 0f, 54f, 54f)
        this.set(GameObject(rect, Color.WHITE))
        this.createBody(BodyDef.BodyType.DynamicBody, world)
    }

    override fun update()
    {
        super.update()
        Gdx.input.apply {
            when
            {
                isKeyPressed(Input.Keys.LEFT) -> body.applyForce(Vector2(-1f * 3, 0f), position, true)
                isKeyPressed(Input.Keys.RIGHT) -> body.applyForce(Vector2(1f * 3, 0f), position, true)
            }
        }
    }
}