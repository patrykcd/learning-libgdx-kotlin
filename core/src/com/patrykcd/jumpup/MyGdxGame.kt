package com.patrykcd.jumpup

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Logger
import com.patrykcd.jumpup.scenes.Gameplay
import com.patrykcd.utilities.Utils

class MyGdxGame : Game()
{
    private lateinit var _spriteBatch: SpriteBatch

    init
    {
        println("MyGdxGame init complete")
    }

    override fun create()
    {
        Gdx.app.logLevel = Logger.DEBUG
        _spriteBatch = SpriteBatch()
        setScreen(Gameplay(_spriteBatch))
    }

    override fun resume()
    {
        super.resume()
    }

    override fun render()
    {
        super.render()
    }

    override fun resize(width: Int, height: Int)
    {
        super.resize(width, height)
    }

    override fun pause()
    {
        super.pause()
    }

    override fun dispose()
    {
        super.dispose()
        Utils.dispose()
    }
}
