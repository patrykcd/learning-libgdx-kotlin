package com.patrykcd.jumpup.helpers

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*

open class GameObject : Sprite
{
    lateinit var body: Body
        private set

    var position: Vector2
        set(value) = body.setTransform(value.x, value.y, 0f)
        get() = body.position

    protected constructor(world: World)

    constructor(texture: Texture, x: Float, y: Float)
    {
        createSprite(texture, x, y)
    }

    constructor(rect: Rectangle, color: Color)
    {
        val pixMap = Pixmap(rect.width.toInt(), rect.height.toInt(), Pixmap.Format.RGBA8888).apply {
            setColor(color)
            fill()
        }

        val pixMapTex = Texture(pixMap)
        pixMap.dispose()

        createSprite(pixMapTex, rect.x, rect.y)
    }

    private fun createSprite(texture: Texture, x: Float, y: Float)
    {
        this.set(Sprite(texture, 0, 0, texture.width, texture.height))
        setPosition(x - (width / 2), y - (height / 2))
    }

    fun createBody(type: BodyDef.BodyType, world: World)
    {
        val halfWidth = width / 2
        val halfHeight = height / 2

        val bodyDef = BodyDef().apply {
            this.position.set((x + halfWidth) / Constants.PPM, (y + halfHeight) / Constants.PPM)
            this.type = type
        }

        body = world.createBody(bodyDef)

        val shape = PolygonShape().apply {
            this.setAsBox(halfWidth / Constants.PPM, halfHeight / Constants.PPM)
        }

        val fixtureDef = FixtureDef().apply {
            this.density = 1f
            this.shape = shape
        }

        // val fixture =
        body.createFixture(fixtureDef)
        shape.dispose()
    }

    open fun update()
    {
        val spriteX = (position.x * Constants.PPM) - (width / 2)
        val spriteY = (position.y * Constants.PPM) - (height / 2)
        setPosition(spriteX, spriteY)
    }
}