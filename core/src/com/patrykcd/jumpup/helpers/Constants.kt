package com.patrykcd.jumpup.helpers

object Constants
{
    const val WIDTH = 480
    const val HEIGHT = 800

    const val PPM = 100
    const val VIRTUAL_WIDTH = WIDTH / PPM
    const val VIRTUAL_HIHT = HEIGHT / PPM
}