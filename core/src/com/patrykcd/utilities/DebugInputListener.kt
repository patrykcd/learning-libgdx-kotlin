package com.patrykcd.utilities

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.OrthographicCamera

class DebugInputListener(private val _camera: OrthographicCamera)
{
    private var _debugSpeed = 10f

    init
    {
        Gdx.app.debug(javaClass.name, "DebugInputListener init")
    }

    fun update(delta: Float)
    {
        var move = _debugSpeed * delta

        when
        {
            Gdx.input.isKeyPressed(Input.Keys.NUM_0) -> _debugSpeed = .1f
            Gdx.input.isKeyPressed(Input.Keys.NUM_1) -> _debugSpeed = 1f
            Gdx.input.isKeyPressed(Input.Keys.NUM_2) -> _debugSpeed = 5f
            Gdx.input.isKeyPressed(Input.Keys.NUM_3) -> _debugSpeed = 10f
            Gdx.input.isKeyPressed(Input.Keys.NUM_4) -> _debugSpeed = 20f
            Gdx.input.isKeyPressed(Input.Keys.NUM_5) -> _debugSpeed = 50f
        }

        if (Gdx.input.isKeyPressed(Input.Keys.Z)) _camera.zoom = 1f
        else if (Gdx.input.isKeyPressed(Input.Keys.Q)) _camera.zoom += move

        val maxZoom = .1f
        val zoomOrNot = if (_camera.zoom >= maxZoom) move else 0f
        if (Gdx.input.isKeyPressed(Input.Keys.E)) _camera.zoom -= zoomOrNot
        if (_camera.zoom < 0) _camera.zoom = maxZoom

        move *= 100f
        if (Gdx.input.isKeyPressed(Input.Keys.W)) _camera.position.y += move
        if (Gdx.input.isKeyPressed(Input.Keys.S)) _camera.position.y -= move
        if (Gdx.input.isKeyPressed(Input.Keys.D)) _camera.position.x += move
        if (Gdx.input.isKeyPressed(Input.Keys.A)) _camera.position.x -= move
    }
}