package com.patrykcd.utilities

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Disposable
import com.patrykcd.jumpup.helpers.Constants

object Utils : Disposable
{
    private var _shapeRenderer: ShapeRenderer
    private var _debugInputListener: DebugInputListener? = null

    init
    {
        Gdx.app.log(javaClass.name, "static ShapeRenderer init")
        _shapeRenderer = ShapeRenderer()
    }

    fun drawCircle(x: Float, y: Float, color: Color, projection: Matrix4)
    {
        _shapeRenderer.color = color
        _shapeRenderer.projectionMatrix = projection
        _shapeRenderer.begin(ShapeRenderer.ShapeType.Line)
        _shapeRenderer.circle(x, y, 25f)
        _shapeRenderer.end()
    }

    fun drawCircle(pos: Vector2, color: Color, projection: Matrix4)
    {
        drawCircle(pos.x, pos.y, color, projection)
    }

    fun drawLine(x1: Float, y1: Float, x2: Float, y2: Float, color: Color, projection: Matrix4)
    {
        _shapeRenderer.color = color// ?: Color.WHITE
        _shapeRenderer.projectionMatrix = projection
        _shapeRenderer.begin(ShapeRenderer.ShapeType.Line)
        _shapeRenderer.line(x1, y1, x2, y2)
        _shapeRenderer.end()
    }

    fun drawLine(start: Vector2, end: Vector2, color: Color, projection: Matrix4)
    {
        drawLine(start.x, start.y, end.x, end.y, color, projection)
    }

    private fun drawFrustum(camera: Camera, color: Color)
    {
        _shapeRenderer.color = color// ?: Color.WHITE
        _shapeRenderer.projectionMatrix = camera.combined
        _shapeRenderer.begin(ShapeRenderer.ShapeType.Line)

//        val frustumPos = Vector2(
//                -(Constants.WIDTH / 2) + camera.position.x,
//                -(Constants.HEIGHT / 2) + camera.position.y)
//        _shapeRenderer.rect(frustumPos.x + 1, frustumPos.y,
//                Constants.WIDTH.toFloat() - 1, Constants.HEIGHT.toFloat() - 1)

        val frustumPos = Vector2(
                -(Constants.WIDTH / 2) + camera.position.x,
                -(Constants.HEIGHT / 2) + camera.position.y)
        _shapeRenderer.rect(frustumPos.x, frustumPos.y,
                Constants.WIDTH.toFloat(), Constants.HEIGHT.toFloat())

        val ratio = 11f
        val frustumPosSmall = Vector2(
                (frustumPos.x + Constants.WIDTH / 2f) - ((Constants.WIDTH / 2f) / ratio),
                (frustumPos.y + Constants.HEIGHT / 2f) - ((Constants.HEIGHT / 2f) / ratio))

        _shapeRenderer.rect(frustumPosSmall.x, frustumPosSmall.y,
                Constants.WIDTH / ratio, Constants.HEIGHT / ratio)

        _shapeRenderer.end()
    }

    fun updateFreeCamera(camera: OrthographicCamera, frustumColor: Color)
    {
        _debugInputListener = _debugInputListener ?: DebugInputListener(camera)
        _debugInputListener.let { it?.update(Gdx.graphics.deltaTime) }
        drawFrustum(camera, frustumColor)
    }

    override fun dispose()
    {
        _shapeRenderer.dispose()
    }
}
