


package com.patrykcd.jumpup;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new MyGdxGame(), config);
	}
}


//package com.patrykcd.jumpup
//
//import android.os.Bundle
//
//import com.badlogic.gdx.backends.android.AndroidApplication
//import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
//
//class AndroidLauncher : AndroidApplication()
//{
//    override fun onCreate(savedInstanceState: Bundle)
//    {
//        super.onCreate(savedInstanceState)
//        val config = AndroidApplicationConfiguration()
//        initialize(MyGdxGame1(), config)
//    }
//}